﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KMG.Scenes
{
    public class SmoothSceneLoad : MonoBehaviour
    {
        public bool sendAwake = true;
        public bool sendOnEnable = true;
        public bool sendStart = true;

        public float allowedLoadTimePerFrameOnSceneLoad = 0.0001f;
        private float startTime = 0.0f;

        public void Load(string scene)
        {
            StartCoroutine(LoadScene(scene));
        }

        private IEnumerator LoadScene(string scene)
        {
            if (!string.IsNullOrWhiteSpace(scene))
            {
                var async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene);
                async.allowSceneActivation = false;

                startTime = Time.realtimeSinceStartup;
                while (!async.isDone)
                {
                    if (async.progress >= 0.9f && !async.allowSceneActivation)
                    {
                        var objects = FindObjectsOfType<GameObject>();

                        // Unity Order of Events
                        // Awake -> OnEnable -> Start

                        // Send Awake
                        if (sendAwake)
                        {
                            yield return SendMessageToObjects(objects, scene, "Awake");
                        }

                        // Send OnEnable
                        if (sendOnEnable)
                        {
                            yield return SendMessageToObjects(objects, scene, "OnEnable");
                        }

                        // Send Start
                        if (sendStart)
                        {
                            yield return SendMessageToObjects(objects, scene, "Start");
                        }
                        yield return null;

                        async.allowSceneActivation = true;
                    }
                    else
                    {
                        yield return null;
                    }
                }
            }
        }

        private IEnumerator SendMessageToObjects(GameObject[] objects, string scene, string message)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                var obj = objects[i];
                if (obj.scene.name == scene && obj) // Check if the object is in the new scene
                {
                    obj.SendMessage(message, SendMessageOptions.DontRequireReceiver);
                }

                if (Time.realtimeSinceStartup - startTime > allowedLoadTimePerFrameOnSceneLoad)
                {
                    yield return null;
                    startTime = Time.realtimeSinceStartup;
                }
            }
        }
    }
}
