﻿using KMG.Scenes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Idolize.Scenes
{
    public class LoadGame : MonoBehaviour
    {
        public float minLoadTime = 3.0f;
        private float timer = 0.0f;

        public string nextScene = "Game";
        private bool nextSceneAllowed = false;

        private SmoothSceneLoad smoothSceneLoad;

        private void Awake()
        {
            smoothSceneLoad = GetComponent<SmoothSceneLoad>();
        }

        public void AllowNextScene()
        {
            nextSceneAllowed = true;
        }

        private void Update()
        {
            timer += Time.unscaledDeltaTime;

            if (timer >= minLoadTime)
            {
                if (nextSceneAllowed)
                {
                    smoothSceneLoad.Load(nextScene);
                    nextSceneAllowed = false;
                }
            }
        }
    }
}
