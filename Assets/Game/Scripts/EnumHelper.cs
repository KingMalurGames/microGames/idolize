﻿using System;
using System.Linq;

namespace KMG.Utility
{
    public static class EnumHelper
    {
        private static System.Random random = new System.Random();

        public static T GetRandom<T>()
            where T : System.Enum
        {
            return Parse<T>(
                Enum.GetNames(typeof(T))
                    [random.Next(0, Enum.GetNames(typeof(T)).Length)]);
        }

        public static T GetRandom<T>(params T[] excludedValues)
            where T : System.Enum
        {
            var listOfT = Enum.GetNames(typeof(T))
                .Where(e => !excludedValues.Contains(Parse<T>(e)));

            return Parse<T>(
                Enum.GetNames(typeof(T))
                    [random.Next(0, listOfT.Count())]);
        }

        public static T Parse<T>(string key)
            where T : System.Enum
        {
            return (T)Enum.Parse(typeof(T), key);
        }

        public static int GetIndex<T>(T key)
            where T : System.Enum
        {
            return GetIndex<T>(key.ToString());
        }

        public static int GetIndex<T>(string key)
            where T : System.Enum
        {
            return Enum.GetNames(typeof(T)).ToList().IndexOf(key);
        }
    }
}
