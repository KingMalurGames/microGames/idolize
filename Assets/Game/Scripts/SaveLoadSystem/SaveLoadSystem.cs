﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem
{
    public abstract class SaveLoadSystem : MonoBehaviour
    {
        public abstract void Save<T>(T objectToSave, string fileName);
        public abstract void Save(UnityEngine.Object objectToSave, string fileName);
        public abstract T Load<T>(string fileName);
    }
}
