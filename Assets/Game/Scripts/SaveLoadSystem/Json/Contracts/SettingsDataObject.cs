﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [CreateAssetMenu(fileName = "SettingsData", menuName = "GameData/SettingsData", order = 3)]
    public class SettingsDataObject : ContractData
    {
        [SerializeField]
        public SettingsData SettingsData;
    }

    [System.Serializable]
    public class SettingsData
    {
        public string Name;
        public string Value;
        public SettingsType Type;
    }
}
