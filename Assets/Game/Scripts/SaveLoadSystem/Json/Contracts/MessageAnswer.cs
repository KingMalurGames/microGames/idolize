﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    public class MessageAnswer : Contract
    {
        [SerializeField]
        public MessageAnswerData[] data;

        public MessageAnswer(List<MessageAnswerDataObject> listOfData)
        {
            data = new MessageAnswerData[listOfData.Count];
            for (int i = 0; i < listOfData.Count; i++)
            {
                data[i] = listOfData[i].MessageAnswerData;
            }
        }
    }
}
