﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [System.Serializable]
    public enum MessageType
    {
        Normal,
        Cute,
        Lifestyle,
        Sexuality,
        Vulgarity,
        Family,
        Fitness,
        Celebrity,
        Item,
        Fantasy,
        Meme,
    }
}
