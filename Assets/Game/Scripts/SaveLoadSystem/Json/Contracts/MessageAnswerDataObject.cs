﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [CreateAssetMenu(fileName = "MessageAnswerData", menuName = "GameData/MessageAnswerData", order = 2)]
    public class MessageAnswerDataObject : ContractData
    {
        [SerializeField]
        public MessageAnswerData MessageAnswerData;
    }

    [System.Serializable]
    public class MessageAnswerData
    {
        public string Text;
        public int Value;
        public MessageType Category;
    }
}
