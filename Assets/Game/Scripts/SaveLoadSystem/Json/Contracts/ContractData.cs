﻿
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    public abstract class ContractData : ScriptableObject
    {
        public int Id;
    }
}
