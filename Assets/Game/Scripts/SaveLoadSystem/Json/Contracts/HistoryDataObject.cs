﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [CreateAssetMenu(fileName = "HistoryData", menuName = "GameData/HistoryData", order = 4)]
    public class HistoryDataObject : ContractData
    {
        [SerializeField]
        public HistoryData HistoryData;
    }

    [System.Serializable]
    public class HistoryData
    {
        public int BodyId;
        public int[] OptionIds;
        public int FollowersGained;
    }
}
