﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [System.Serializable]
    public enum SettingsType
    {
        Integer,
        String,
        Guid,
        Boolean,
    }
}
