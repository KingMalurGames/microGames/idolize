﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [System.Serializable]
    public class History : Contract
    {
        public HistoryData[] data;

        public History(List<HistoryDataObject> listOfData)
        {
            data = new HistoryData[listOfData.Count];
            for (int i = 0; i < listOfData.Count; i++)
            {
                data[i] = listOfData[i].HistoryData;
            }
        }
    }
}
