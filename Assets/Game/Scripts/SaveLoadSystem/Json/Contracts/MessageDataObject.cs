﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [CreateAssetMenu(fileName = "MessageData", menuName = "GameData/MessageData", order = 1)]
    public class MessageDataObject : ContractData
    {
        [SerializeField]
        public MessageData MessageData;
    }

    [System.Serializable]
    public class MessageData
    {
        public string Text;
        public int Pick;
        public int Value;
        public MessageType Category;
    }
}
