﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [System.Serializable]
    public class Settings : Contract
    {
        [SerializeField]
        public SettingsData[] data;

        public Settings(List<SettingsDataObject> listOfData)
        {
            data = new SettingsData[listOfData.Count];
            for (int i = 0; i < listOfData.Count; i++)
            {
                data[i] = listOfData[i].SettingsData;
            }
        }
    }
}
