﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    public class ContractController : MonoBehaviour
    {
        [Header("Filenames")]
        public string historyFileName = "History";
        public string messagesFileName = "Messages";
        public string messageAnswersFileName = "MessageAnswers";
        public string settingsFileName = "Settings";

        [Header("References")]
        public JsonSaveLoadSystem jsonSaveLoadSystem;

        private History cachedHistory;
        private Message cachedMessages;
        private MessageAnswer cachedMessageAnswers;
        private Settings cachedSettings;

        public void Save<Tdata>(Tdata data, string saveName)
            where Tdata : Contract
        {
            jsonSaveLoadSystem.Save(data, saveName);
        }

        public T Load<T>(string saveName)
            where T : Contract
        {
            return jsonSaveLoadSystem.Load<T>(saveName);
        }

        public void SaveMessages(Message data)
        {
            cachedMessages = data;
            Save(data, messagesFileName);
        }

        public Message LoadMessages()
        {
            if (cachedMessages == null)
            {
                cachedMessages = Load<Message>(messagesFileName);
            }
            return cachedMessages;
        }

        public void SaveMessageAnswers(MessageAnswer data)
        {
            cachedMessageAnswers = data;
            Save(data, messageAnswersFileName);
        }

        public MessageAnswer LoadMessageAnswers()
        {
            if (cachedMessageAnswers == null)
            {
                cachedMessageAnswers = Load<MessageAnswer>(messageAnswersFileName);
            }
            return cachedMessageAnswers;
        }

        public void SaveSettings(Settings data)
        {
            cachedSettings = data;
            Save(data, settingsFileName);
        }

        public Settings LoadSettings()
        {
            if (cachedSettings == null)
            {
                cachedSettings = Load<Settings>(settingsFileName);
            }
            return cachedSettings;
        }

        public void SaveHistory(History data)
        {
            cachedHistory = data;
            Save(data, historyFileName);
        }

        public History LoadHistory()
        {
            if (cachedHistory == null)
            {
                cachedHistory = Load<History>(historyFileName);
            }
            return cachedHistory;
        }
    }
}
