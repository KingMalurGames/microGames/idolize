﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.Events;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [System.Serializable]
    public class ContractInitializer : MonoBehaviour
    {
        public UnityEvent onContractsInitializedEvent;

        public List<MessageDataObject> messages;
        public List<MessageAnswerDataObject> messageAnswers;
        public List<SettingsDataObject> settings;

        private ContractController contractController;

        private void Awake()
        {
            contractController = GetComponent<ContractController>();
        }

        private void Start()
        {
            // Check if contract-data is saved as json and save new ones
            CheckContracts();

            onContractsInitializedEvent?.Invoke();
        }

        private void CheckContracts()
        {
            if (!CheckForContractData(messages))
            {
                SaveContract(messages);
            }

            if (!CheckForContractData(messageAnswers))
            {
                SaveContract(messageAnswers);
            }

            if (!CheckForContractData(settings))
            {
                SaveContract(settings);
            }
        }

        private bool CheckForContractData(List<MessageDataObject> data)
        {
            try
            {
                var loadedData = contractController.LoadMessages();
                return loadedData.data.ToList().Equals(data);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private bool CheckForContractData(List<MessageAnswerDataObject> data)
        {
            try
            {
                var loadedData = contractController.LoadMessageAnswers();
                return loadedData.data.ToList().Equals(data);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private bool CheckForContractData(List<SettingsDataObject> data)
        {
            try
            {
                var loadedData = contractController.LoadSettings();
                return loadedData.data.ToList().Equals(data);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private void SaveContract(List<MessageDataObject> data)
        {
            var saveData = new Message(data);
            try
            {
                contractController.SaveMessages(saveData);
            }
            catch (System.Exception)
            { }
        }

        private void SaveContract(List<MessageAnswerDataObject> data)
        {
            var saveData = new MessageAnswer(data);
            try
            {
                contractController.SaveMessageAnswers(saveData);
            }
            catch (System.Exception)
            { }
        }

        private void SaveContract(List<SettingsDataObject> data)
        {
            var saveData = new Settings(data);
            try
            {
                contractController.SaveSettings(saveData);
            }
            catch (System.Exception)
            { }
        }
    }
}
