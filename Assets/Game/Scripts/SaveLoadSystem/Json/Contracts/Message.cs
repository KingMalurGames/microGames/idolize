﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json.Contracts
{
    [System.Serializable]
    public class Message : Contract
    {
        [SerializeField]
        public MessageData[] data;

        public Message(List<MessageDataObject> listOfData)
        {
            data = new MessageData[listOfData.Count];
            for (int i = 0; i < listOfData.Count; i++)
            {
                data[i] = listOfData[i].MessageData;
            }
        }
    }
}
