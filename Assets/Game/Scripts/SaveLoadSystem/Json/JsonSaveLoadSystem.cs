﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace KMG.SaveLoadSystem.Json
{
    public class JsonSaveLoadSystem : SaveLoadSystem
    {
        public override void Save<T>(T objectToSave, string fileName)
        {
            SaveToFile<T>(objectToSave, fileName);
        }

        public override void Save(Object objectToSave, string fileName)
        {
            SaveToFile<Object>(objectToSave, fileName);
        }

        private void SaveToFile<T>(T objectToSave, string fileName)
        {
            string path = Path.Combine(Application.persistentDataPath, "Save");
            Directory.CreateDirectory(path);
            string filePath = Path.Combine(path, fileName + ".json");

            bool isSaved = false;
            try
            {
                string json = JsonUtility.ToJson(objectToSave);
                File.WriteAllText(filePath, json);
                isSaved = true;
            }
            catch (System.Exception exception)
            {
                Debug.Log("Save failed. Error: " + exception.Message);
                isSaved = false;
            }

            if (!isSaved)
            {
                throw new System.Exception();
            }
        }

        public override T Load<T>(string fileName)
        {
            string path = Path.Combine(Application.persistentDataPath, "Save", fileName + ".json");

            T returnValue = default(T);

            bool isLoaded = false;
            try
            {
                string json = File.ReadAllText(path);
                returnValue = JsonUtility.FromJson<T>(json);
                isLoaded = true;
            }
            catch (System.Exception exception)
            {
                Debug.Log("Load failed. Error: " + exception.Message);
                isLoaded = false;
            }

            if (!isLoaded)
            {
                throw new System.Exception();
            }

            return returnValue;
        }
    }
}
