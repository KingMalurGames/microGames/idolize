﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Idolize.UI.MainMenu
{
    public class UiController : MonoBehaviour
    {
        private PageController pageController;

        private void Awake()
        {
            pageController = FindObjectOfType<PageController>();
        }

        public void OnStartClick()
        {
            // Decide if new player has to be created -> Setup
            // or old has to be loaded -> Feed
            // For now always open Setup
            pageController.OpenPage(Pages.Setup);
        }
    }
}
