﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Idolize.UI
{
    public enum Pages
    {
        None,
        Feed,
        History,
        MainMenu,
        Settings,
        Setup,
    }
}
