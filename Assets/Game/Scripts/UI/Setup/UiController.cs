﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Idolize.UI.Setup
{
    public class UiController : MonoBehaviour
    {
        private PageController pageController;

        private void Awake()
        {
            pageController = FindObjectOfType<PageController>();
        }

        public void OnStartClick()
        {
            // Has to check if player entered a name
            // For now always open Feed without checking
            pageController.OpenPage(Pages.Feed);
        }
    }
}
