﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Idolize.UI
{
    [System.Serializable]
    public class PagesTransformEntry
    {
        public Pages pageType;
        public Transform pageTransform;
    }
}
