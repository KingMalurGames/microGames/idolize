﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Idolize.UI.ActionButtons
{
	public class UiController : MonoBehaviour
	{
		private PageController pageController;

		public Button[] buttons;
		public Pages[] deactivatingPages;

		private void Awake()
		{
			pageController = FindObjectOfType<PageController>();
		}

		public void OnPageChanged()
		{
			bool interactable = true;
			if (deactivatingPages.Contains(pageController.CurrentPage))
			{
				interactable = false;
			}

			foreach (var b in buttons)
			{
				b.interactable = interactable;
			}
		}
	}
}
