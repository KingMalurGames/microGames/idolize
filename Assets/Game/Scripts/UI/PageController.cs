﻿using KMG.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Idolize.UI
{
    public class PageController : MonoBehaviour
    {
        public UnityEvent onPageChangedEvent;

        public Pages CurrentPage { get; private set; }

        public PagesTransformEntry[] pages;

        private void Awake()
        {
            CurrentPage = Pages.None;
        }

        private void Start()
        {
            OpenPage(Pages.MainMenu);
        }

        public void OpenPage(Pages page)
        {
            if (page == CurrentPage)
            {
                return;
            }

            var deactivatedPages = pages.Where(p => p.pageType != page);
            foreach (var p in deactivatedPages)
            {
                p.pageTransform.gameObject.SetActive(false);
            }

            var activatedPages = pages.Where(p => p.pageType == page);
            foreach (var p in activatedPages)
            {
                p.pageTransform.gameObject.SetActive(true);
            }

            CurrentPage = page;
            onPageChangedEvent?.Invoke();
        }

        public void OpenPage(string nextPage)
        {
            OpenPage(EnumHelper.Parse<Pages>(nextPage));
        }
    }
}
